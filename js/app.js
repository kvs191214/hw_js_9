/*
1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.

Використайте 2 способи для пошуку елементів.

Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 */

const getClassFuture = document.getElementsByClassName('feature');
const classFutureByQuery = document.querySelectorAll('.feature');

console.log(getClassFuture);
console.log(classFutureByQuery);

for (let item of getClassFuture) {
    item.setAttribute('text-align', 'center')
}

classFutureByQuery.forEach((element) => element.setAttribute('text-align', 'center'));

/*
2. Змініть текст усіх елементів h2 на "Awesome feature".
 */

const allH2 = document.querySelectorAll('h2');

allH2.forEach((element) => element.innerText = 'Awesome feature');

/*
3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 */

const getFutureTitle = document.querySelectorAll('.feature-title');

getFutureTitle.forEach((element) => element.innerText += ' !');